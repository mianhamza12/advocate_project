from django.contrib import admin
from .models import company_db,advocate_db
# Register your models here.

@admin.register(company_db)
class show(admin.ModelAdmin):
    list_display = ['id','name','logo']

@admin.register(advocate_db)
class show2(admin.ModelAdmin):
    list_display = ['id','username','adress','profile','twitter','facebook']


