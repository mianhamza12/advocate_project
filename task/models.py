from django.db import models

# Create your models here.
class company_db(models.Model):
    name=models.CharField(max_length=100)
    logo=models.URLField()
    # company = models.ForeignKey(advocate_db, on_delete=models.CASCADE, related_name='company')
    def __str__(self):
        return self.name

class advocate_db(models.Model):
    username=models.CharField(max_length=120)
    adress = models.CharField(max_length=120)
    profile = models.TextField(max_length=120)
    twitter=models.URLField()
    facebook = models.URLField()
    company=models.ForeignKey(company_db,on_delete=models.CASCADE,related_name='company')

    def __str__(self):
        return self.username

