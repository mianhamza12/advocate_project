from rest_framework import serializers
from .models import company_db,advocate_db


class company_sri(serializers.ModelSerializer):
    # company = serializers.StringRelatedField(many=True,read_only=True)
    class Meta:
        model=company_db
        fields=['name','logo']

class advocate_sri(serializers.ModelSerializer):
    # company1=serializers.StringRelatedField(many=True,read_only=True)
    company = company_sri()
    # links = serializers.SerializerMethodField()
    id=serializers.IntegerField(read_only=True)
    class Meta:
        model=advocate_db
        fields=['id','username','adress','profile','twitter','facebook','company']


class advocatefu_sri(serializers.ModelSerializer):
    class Meta:
        model=advocate_db
        fields=['username','adress','profile','twitter','facebook','company']

    def create(self, validated_data):
        return advocate_db.objects.create(**validated_data)


class companyfu_sri(serializers.ModelSerializer):
    class Meta:
        model=company_db
        fields=['name','logo']


